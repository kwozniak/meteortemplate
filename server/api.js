//Dokumentacja na http://github.differential.io/reststop2/

// Global configuration
RESTstop.configure({
    use_auth: true
});

// // Maps to: /api/get_user
// RESTstop.add('get_user', function() {
//     if (!this.user) {
//         return {
//             is_loggedin: false
//         };
//     }

//     return {
//         is_loggedin: true,
//         username: this.user.username
//     };
// });

// // Maps to, for example: /api/get_num/42
// RESTstop.add('get_num/:num?', function() {
//     if (!this.params.num) {
//         return [403, {
//             success: false,
//             message: 'You need a num as a parameter!'
//         }];
//     }

//     return this.params.num;
// });

// // Maps to: /api/posts
// RESTstop.add('posts', {
//     require_login: true
// }, function() {
//     var posts = [];

//     Posts.find({
//         owner_id: this.user._id
//     }).forEach(function(post) {

//         // Modify the post here...

//         posts.push(post);
//     });

//     return posts;
// });